from flask import Flask, render_template, request
import pandas as pd
from pyspark.sql import SparkSession
from IPython.core.display import HTML


app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/uploadpd", methods=["POST"])
def upload_pd():
    file = request.files["file"]
    if not file:
        return "No file selected"

    file.save("uploaded_file.csv")
    data = pd.read_csv("uploaded_file.csv")
    return data.to_html()

@app.route("/uploadspark", methods=["POST"])
def upload_spark():
    file = request.files["file"]
    if not file:
        return "No file selected"

    file.save("uploaded_file.csv")
    spark = SparkSession.builder \
        .master("local") \
        .appName("Data Exploration") \
        .getOrCreate()
    data = spark.read.csv("uploaded_file.csv", sep=',', header=True)
    html_data = data.show()
    return HTML(html_data)


if __name__ == "__main__":
    app.run(debug=True)