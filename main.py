import pandas as pd

from pyspark.sql import SparkSession
from pyspark.sql.functions import desc
from pyspark.sql.functions import sum,avg,max,col
file_path="./data/conso-elec-gaz-annuelle-par-naf-agregee-iris.csv"

spark= SparkSession.builder \
    .master("local") \
    .appName("Data Exploration") \
    .getOrCreate()


df = spark.read.csv(file_path, sep =';', header = True)
df.printSchema()
df.show()
df=df[['operateur','annee','filiere','libelle_categorie_consommation','libelle_grand_secteur','conso','libelle_region']]
df.show()
"""
df.createOrReplaceTempView("TAB")
spark.sql("SELECT DISTINCT operateur FROM TAB ORDER BY operateur ASC").show(181)
df.groupBy('annee','libelle_commune')\
    .agg(avg("conso").alias("moyen de conso"))\
    .filter(col('annee')=="2020")\
    .sort(desc("moyen de conso")).show()

df.groupBy('annee','libelle_departement')\
    .agg(avg("conso").alias("moyen_de_conso"))\
    .filter(col('annee')=="2020")\
    .sort(desc('moyen_de_conso')).show()
    
df.groupBy('libelle_region','libelle_categorie_consommation')\
    .agg(avg("conso").alias("moyen_de_conso"))\
    .sort(desc('moyen_de_conso')).show(100)    
"""


df.groupBy('operateur','libelle_grand_secteur')\
    .agg(
    sum("conso").alias("tatal_consomation"),
    avg("conso").alias("moyenne_consomation")
      )\
    .sort(desc('tatal_consomation')).show(100)