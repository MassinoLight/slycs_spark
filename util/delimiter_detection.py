import csv

def get_delimiter(file_path):
    with open(file_path) as f:
        dialect = csv.Sniffer().sniff(f.read(1024))
    return dialect.delimiter