import os
import pandas as pd
import psutil
from util.delimiter_detection import get_delimiter


def get_ram_avaible():
    memory = psutil.virtual_memory()
    total = memory.total / (1024.0 ** 3)
    available = memory.available / (1024.0 ** 3)
    # pour obtenir la mémoire restante
    memory_free = available
    print(memory_free,"Go disponible")
    return memory_free

#Calcule ne nombre de lignes d'un fichier
def count_file_line(file_path):
    with open(file_path, "r") as f:
        num_lines = 0
        for line in f:
            num_lines += 1
        return num_lines


#methode1 on fait une régle de 3 avec une partie du fichier et le nombre de ligne totla
def get_memory_usage_part_file(file_path):
    line=count_file_line(file_path)
    df = pd.read_csv(file_path, nrows=10, sep=get_delimiter(file_path))

    mem_usage_10_lines = df.memory_usage(deep=True).sum() / (1024 * 1024)
    memory_usage=((line*mem_usage_10_lines)/10)/1024
    print("L'utilisation de la mémoire est de : ", memory_usage, " Go")
    return memory_usage

#methode2 on calcule la taille colonne/colonne
def get_memory_usage_with_col_type(file_path):

    df = pd.read_csv(file_path, nrows=1, sep=get_delimiter(file_path))
    return count_file_line(file_path)*sum(df.memory_usage(deep=True).tolist())/ 1e9